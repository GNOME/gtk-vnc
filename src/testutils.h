/*
 * GTK VNC Widget, test utils
 *
 * Copyright (C) 2024  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 */

#ifndef VNC_TEST_UTILS_H__
#define VNC_TEST_UTILS_H__


#include <glib.h>

GBytes *bytes_from_hex(const gchar *hex);
char *bytes_to_hex(GBytes *val);

void test_assert_cmpbytes(const guint8 *want, size_t wantlen,
                          const guint8 *got, size_t gotlen);

#endif
